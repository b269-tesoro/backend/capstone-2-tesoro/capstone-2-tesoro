const jwt = require("jsonwebtoken");

const secret = "Jf0fU6Ud74i8C351pj4FDTMh3opwxzBgZL4OT737toSl012ZTn"

// User Token creation
module.exports.createAccessToken = (user) =>
{
	const data =
	{
		id: user._id,
		email: user.email,
		username: user.username,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

// Token Verification for regular users
module.exports.verify = (req, res, next) =>
{
	let token = req.headers.authorization;
	if(typeof token !== "undefined")
	{
			console.log(token);
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (err, data) =>
			{
				if (err)
				{
					return res.send({error: "Sorry, this feature is only available to registered users. Please log in or sign up to access it."});
				}
				else
				{
					next();
				}
			});
	}
	else
	{
		return res.send({error: "Sorry, this feature is only available to registered users. Please log in or sign up to access it."});
	}
};

// Token decryption
module.exports.decode = (token) =>
{
	if(typeof token !== "undefined")
	{
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) =>
		{
			if (err)
			{
				return null;
			}
			else
			{
				return jwt.decode(token, {complete: true}).payload;
			}
		});
	}
	else
	{
		return null;
	}
};	

// Token verification for admin only commands
//  Since this project involves a lot of functions that are limited to admin only, this avoids redeclaring "if()" statements on each controller and route that only the admin can see. the controller will only check for admin status if the app is supposed to look different to users and admins on the same route.
module.exports.verifyAdmin = (req, res, next) =>
{
	let token = req.headers.authorization;
	if(typeof token !== "undefined")
	{
			console.log(token);
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (err, data) =>
			{
				if (err || !data.isAdmin)
				{
					return res.send({error: "Access denied. You must be logged in as admin to perform this action. If you believe this is an error, please contact our support team."});
				}
				else
				{
					if (err)
					{
						return null;
					}
					else
					{
						next();
						return jwt.decode(token, {complete: true}).payload;
					}
				};
			});
	}
	else
	{
		return res.send({error: "Access denied. You must be logged in as admin to perform this action. If you believe this is an error, please contact our support team."});
	}
};
