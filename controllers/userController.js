// Dependencies
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller function for user registration
module.exports.registerNewUser = async(reqBody) =>
{
	try
	{
		// checking if user email is already registered
		let checkEmail = await User.find({email:reqBody.email}).then(emailResult =>
			{
				if(emailResult.length > 0) {return true;}
				else {return false};
			});

		// checking if user email is already registered
		let checkUsername = await User.find({username:reqBody.username}).then(usernameResult =>
			{
				if(usernameResult.length > 0) {return true;}
				else {return false};
			});

		// if either email or username has already been registered, it returns an error message
		if (checkEmail || checkUsername)
		{
			return "email or username already exists. Please proceed to log in.";
		}
		// register new user if it's not yet registered
		else
		{
			let newUser = new User
			({
				email: reqBody.email,
				username: reqBody.username,
				password: bcrypt.hashSync(reqBody.password, 10)
			});
			// validating for required fields
			await newUser.validate();

			return newUser.save().then((user,error) =>
			{
				if(error)
				{
					return "Registration failed.";
				}
				else
				{
					return "Registration successful."
				}
			});
		}
	}
	catch (error)
	{
		return error.message;
	}
	
};

// Controller function for user authentication/login using either email or password
module.exports.userLogin = (reqBody) =>
{
	// tries logging in with email
	return User.findOne({email: reqBody.email}).then(result =>
	{
		// if the email inputted by user was not found, OR if the user did not input an email, the result will be null
		if (result == null)
		{
			// now checking if the user inputted a username
			return User.findOne({username: reqBody.username}).then(result =>
			{
				// if the user did not input a username, or inputted one that does not exist, the result will be null
				if (result == null)
				{
					// for security reasons, the error message does not show that the user/email is not registered even if that is what is causing the error
					return "Incorrect email/username or password."
				}
				else
				{
					const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

					if(isPasswordCorrect)
					{
						return {"access token": auth.createAccessToken(result)};
					}
					else
					{
						return "Incorrect email/username or password.";
					}
				}
			});
			
		}
		else
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect)
			{
				return {"access token": auth.createAccessToken(result)};
			}
			else
			{
				return "Incorrect username or password.";
			}
		}
	});
};

// Controller for retrieving user details
module.exports.getUserProfile = (reqParams, userData) =>
{
	if(!userData.isAdmin)
	{
		// checking to make sure that the logged in user is accessing their own info
		if(userData.id === reqParams.id)
		{
			return User.findById(userData.id, {
				_id: 0,
				password: 0,
				isAdmin: 0
			}).then(result => {return result;});
		}
		// even if the code can run to show the authenticated user's information instead of the id on the route, i decided to return an error message to make sure that the router link id can never mismatch the information returned to the user if they are not admin. This makes the API cleaner and less prone to misinformation.
		else
		{
			let message = Promise.resolve("Unauthorized access: You are not allowed to access the profile for this user. Please ensure that you are logged in as the correct user and try again.");
			return message;
		}
	}
	else
	{
		return User.findById(reqParams.id).then(result => {return result;});
	}
};

// Controller for retrieving all users (Admin Only)
module.exports.getAllUsers = () =>
{
	return User.find().then(result =>{return result;});
};

// Controller function for elevating user to Admin
module.exports.elevate = (reqParams) =>
{
	return User.findById(reqParams.id).then(user => 
		{
			if(!user.isAdmin)
			{
				return User.findByIdAndUpdate(reqParams.id, {isAdmin:true}).then(adminUser =>
				{
					return `${adminUser.username} has been elevated to Admin.`
				});
			}
			else
			{
				return `${user.username} is already an admin.`;
			}
		});
};

// Controller function for retrieving authenticated user's orders
module.exports.getUserOrders = (reqParams, userData) =>
{
	if(userData.isAdmin)
	{
		return Order.find({userId: reqParams.id}).then(orders => {return orders;});
	}
	else
	{
		// checking to make sure that the logged in user is accessing their own info
		if(userData.id === reqParams.id)
		{
			return Order.find({userId: reqParams.id}).then(orders => {return orders;});
		}
		// even if the code can run to show the authenticated user's information instead of the id on the route, i decided to return an error message to make sure that the router link id can never mismatch the information returned to the user if they are not admin. This makes the API cleaner and less prone to misinformation.
		else
		{

			let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
			return message;
		}
	}
};

// Controller function for adding to cart
// NOTE: this function does both add to Cart AND update quantity.
module.exports.addToCart = (reqParams, data) => 
{
	try
	{
		if (!data.isAdmin) 
		{
			// defaulting the value of quantity to 1 in case user does not input
			let quantity = 1;
			if (data.userId === reqParams.id) 
			{
				return Product.findById(data.reqBody.productId).then((product) => 
				{
					if (!product) 
					{
						return ("Product not found");
					}

					const finalPrice = product.finalPrice;
					// if user provided a quantity, update quantity
					if(data.reqBody.quantity)
					{
						quantity = data.reqBody.quantity;
					}
					//checking quantity against available stock to prevent adding more products than available to cart

					if(product.stock >= quantity)
					{
						const computedSubtotal = finalPrice * quantity;

						return User.findById(data.userId).populate("cart.productId").then((user) => 
						{
							// checking if the product already exists in the cart to prevent duplication
							const cartItemIndex = user.cart.findIndex((item) => item.productId._id.toString() === data.reqBody.productId);

							// If the product already exists in the cart, update it
							if (cartItemIndex >= 0) 
							{
								user.cart[cartItemIndex].quantity = quantity;
								user.cart[cartItemIndex].subTotal = computedSubtotal;
							}

							// If the product is not in the cart, add it
							else 
							{
								// if user provided a quantity, update quantity
								if(data.reqBody.quantity)
								{
									quantity = data.reqBody.quantity;
								}

								user.cart.push({
								productId: product._id,
								quantity: quantity,
								subTotal: computedSubtotal,
								});
							}

							return user.save().then((user, error) => 
							{
								if (error) 
								{
									return "Cart update failed.";
								} 
								else 
								{
									return({message: "Cart updated successfully."}); 
								}
							});
						});
					}
					else
					{
						throw new Error ("Cannot order more than available stock.")
					}
					
				})
			.catch((err) => 
			{
				return { error: err.message };
			});
			} 
			else 
			{
				let message = 
				Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
				return message;
			}
		} 
		else 
		{
			return "Admin users are not authorized to check out products.";
		}
	}
	catch(error)
	{
		return error.message;
	}
	
};

// Controller function for removing product from cart
module.exports.removeFromCart = (reqParams, data) => 
{
	if (!data.isAdmin) 
	{
		if(data.userId === reqParams.id) 
		{
			return User.findById(data.userId).populate("cart.productId").then((user) => 
			{
				// Check if the product exists in the cart
				const productIndex = user.cart.findIndex((item) => item.productId._id.toString() === data.reqBody.productId);

				if (productIndex === -1) 
				{
					// Product not found in cart
					return ("Product not found in cart.");
				} 
				else 
				{
					// Remove the product from the cart
					user.cart.splice(productIndex, 1);

					return user.save().then((user, error) => 
					{
						if(error) 
						{
							return "Could not remove product from cart."
						} else {
							return ({message: "product successfully removed from cart.", Cart: user.cart});
						}
					})
				}
			});
		} 
		else 
		{
			let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
			return message;
		}
	} 
	else 
	{
		return "Admin users are not authorized to modify carts.";
	}
};



// Controller for viewing Cart
// NOTE: I decided to let admins view cart since this won't allow them to check out items
module.exports.getCart = (reqParams, userData) =>
{
	if(userData.id === reqParams.id || userData.isAdmin)
	{
		return User.findById(reqParams.id).populate("cart.productId").then(user => 
		{
			let totalAmount = 0;
			let i = 0;
			user.cart.forEach((item) =>
			{
			console.log(item.quantity);

				// updating subTotals of each product in case any were updated
				user.cart[i].productId.subTotal = user.cart[i].productId.finalPrice * item.quantity
				item.subTotal = user.cart[i].productId.subTotal;

				totalAmount += item.subTotal;
				i++;
			});

			return user.save().then((user, error) =>
			{
				if(error)
				{
					return error.message;
				}
				else
				{
					return({
						message: `${user.username}'s cart`,
						cart: user.cart,
						"Total Amount": totalAmount
					});
				}
			});
			
		})
	}
	else 
	{
		let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
		return message;
	}
};

// Controller function for checking out all items in cart
module.exports.checkOutCart = async (reqParams, userData) =>
{
	try
	{
		if(!userData.isAdmin)
		{
			if(reqParams.id === userData.id)
			{
				// Getting the user's cart information first
	 			const user = await User.findById(userData.id).populate('cart.productId')

 				let i = 0;
 				user.cart.forEach((item) =>
 				{
 					// updating subTotals of each product in case any were updated
 					user.cart[i].productId.subTotal = user.cart[i].productId.finalPrice * item.quantity
 					item.subTotal = user.cart[i].productId.subTotal;

 					i++;
 				});
 				await user.save().then((user, error) =>
 				{
 					if(error)
 					{
 						return error.message;
 					}
 					else
 					{
 						return user;
 					}
 				});

	 			// check if cart is empty
	 			if(user.cart == [])
	 			{
	 				throw new Error("Your cart is empty.")
	 			}
	 			// Use an initial .map method to make sure all quantities in the cart are less than or equal to the product's stock
	 			// NOTE: although adding to cart already prevents the user from adding more products than available stock, the checkOut function still checks against stock in case the product was updated by other means (i.e. different users checking out the prodcut first so now there are more quantities in the cart than there are in stock)
	 			const productCheck = await user.cart.map((cartItem) =>
	 			{
	 				const productCheck = cartItem.productId;
	 				if(cartItem.quantity > productCheck.stock)
	 				{
	 					throw new Error("Cannot order more than available stock.")
	 				}
	 			})

	 			// Uses the .map method to iterate adding each product in the cart to the order
	 			// For each iteration, it also updates the checked out product's remaining stock and total purchase count
				const products = await user.cart.map((cartItem) => 
				{
					// updating product information
					const product = cartItem.productId;
					updatedStock = product.stock - cartItem.quantity;
					updatedCount = product.buyCount + cartItem.quantity;
					
					Product.findByIdAndUpdate(product._id,{stock: updatedStock, buyCount: updatedCount}).then(product =>
					{
						return product.save()
					})

					// returning product info snapshot to be saved in the new order
					return (
					{
						productId: product._id,
						name: product.name,
						description: product.description,
						price: product.price,
						salePercent: product.salePercent,
						quantity: cartItem.quantity,
						finalPrice: product.finalPrice,
						subTotal: cartItem.subTotal,
					});	
				});

				// Computes the final total amount by reducing subtotals for each product
				// NOTE: I have elected not to save the totalAmount value in the cart model. This is because this number doesn't really need to be stored as long as the user can still see the total amount when viewing the cart. However, this is not the case for Orders; Orders store the appropriate snapshot of the totalAmount.
				const totalAmount = user.cart.reduce((acc, cartItem) => acc + cartItem.subTotal, 0);

				const order = new Order(
				{
					userId: userData.id,
					products: products,
					totalAmount: totalAmount,
					lastUpdatedOn: new Date(),
				});
				// updates and saves the newly created order
				await order.save();

				// adds the new order's id in the order list of the user
				user.orders.push({ orderId: order._id });

				// empties the cart now that all items have been checked out
				user.cart = [];
				
				// saves all changes to the user data
				await user.save();

				return ({message: "order successfully placed!", order: order});
			}
			else
			{
				let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
				return message;
			}
		}
		else
		{
			return "Admin users are not authorized to purchase items.";
		}
	}
	catch(error)
	{
		return error.message
	}
}
