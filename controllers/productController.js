// Dependencies
const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");

// Controller function for creating new product (Admin only)
module.exports.createNewProduct = async (reqBody) =>
{
	try
	{
		// computing for the value of finalPrice
		let salePrice = reqBody.price *(1 - reqBody.salePercent/100);
		let newProduct = new Product
		({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			category: reqBody.category,
			// gives the admin the option to declare values at item creation, but the method will push through even if these fields are left blank since they weren't set to be required on the Product schema
			salePercent: reqBody.salePercent,
			finalPrice: salePrice,
			stock: reqBody.stock,
			isActive: reqBody.isActive
		});

		// validating for required fields
		await newProduct.validate();

		return newProduct.save().then((product, error) =>
		{
			if(error)
			{
				return "Product creation failed."
			}
			else
			{
				return ({message: "Product creation successful.", product: product});
			}
		});
	}
	catch(error)
	{
		return error.message;
	}
	
};

// Controller function for retrieving all active products
module.exports.getAllActiveProducts = () =>
{
	// return only limited information
	return Product.find({isActive: true}, 
	{
		isActive: 0,
		createdOn: 0
	}).then(products => {return products});
};

// Controller function for retrieving one product
module.exports.getProduct = (reqParams) =>
{
	return Product.findById(reqParams.productId).then(result =>{return result;});
}

// Controller function for updating product information (admin only)
module.exports.updateProduct = (reqParams, reqBody) =>
{
	let updatedProduct =
	{
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		category: reqBody.category,
		salePercent: reqBody.salePercent,
		stock: reqBody.stock
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) =>
	{
		if (error)
		{
			return error;
		}
		else
		{
			// updating finalPrice in case the user updated the price or salePercent fields
			let newPrice = product.price;
			let newSalePercent = product.salePercent;

			if(reqBody.price)
			{
				newPrice = reqBody.price
			}
			if(reqBody.salePercent)
			{
				newSalePercent = reqBody.salePercent
			}

			let salePrice = newPrice *(1 - newSalePercent/100);

			return Product.findByIdAndUpdate(reqParams.productId, {finalPrice: salePrice}).then((product,error) =>
			{
				if(error)
				{
					return error;
				}
				else
				{
					return ({message: `the product ${product.name} has been successfully updated.`});
				}			
			})	
		}
	});
};

// Controller function for archiving products (admin only)
module.exports.archiveProduct = (reqParams) =>
{
	return Product.findByIdAndUpdate(reqParams.productId, {isActive: false}).then((product,error) =>
	{
		if (error)
		{
			return error;
		}
		else
		{
			return ({message: `the product ${product.name} has been successfully archived.`});
		}
	});
}

// Controller function for getting archived products (admin only)
module.exports.getArchivedProducts = () =>
{
	return Product.find({isActive:false}).then(result => {return result;});
};

// Controller function for restoring specific archived product (admin only)
module.exports.restoreProduct = (reqParams) =>
{
	return Product.findByIdAndUpdate(reqParams.productId, {isActive: true}).then((product,error) =>
	{
		if (error)
		{
			return error;
		}
		else
		{
			return ({message: `the product ${product.name} has been successfully restored.`});
		}
	});
}

// Controller function for directly checking out a product without adding to cart as non-admin user(basic capstone requirement)
module.exports.buyNow = async (reqParams, data) =>
{
	try
	{
		// checking if user is non-admin
		if(!data.isAdmin)
		{
			// Creating new order
			const savedOrder = await Product.findById(reqParams.productId).then(product =>
			{
				// checking if the available stock is enough for the quantity
				let quantity = 1

				if(data.reqBody.quantity)
				{
					quantity = data.reqBody.quantity
				}
				if(quantity <= product.stock)
				{
					// updating product information
					updatedStock = product.stock - quantity;
					updatedCount = product.buyCount + quantity;
					
					Product.findByIdAndUpdate(product._id,{stock: updatedStock, buyCount: updatedCount}).then(product =>
					{
						return product.save()
					})

					let computedSubTotal = product.finalPrice * quantity;
					const newOrder = new Order
					({
						userId: data.userId,
						// takes a screenshot of the products relevant information at the time of purchase
						products: 
						[{
							productId: reqParams.productId,
							name: product.name,
							description: product.description,
							price: product.price,
							salePercent: product.salePercent,
							quantity: quantity,
							finalPrice: product.finalPrice,
							subTotal: computedSubTotal
						}],
						// NOTE: the "Buy Now" feature instantly checks out one product only, therefore the totalAmount will always be equal to the subTotal. This is not the case for checking out from a user's cart and the totalAmount computation is applied accordingly.
						totalAmount: computedSubTotal
					});

					return newOrder.save().then((order,error) =>
						{
							if (error) {return false;}
							else {return order;}
						});
				}
				else
				{
					throw new Error("Cannot order more than available stock.");
				}
			})
			
			// Updating user's order
			const updateUser = await User.findById(data.userId).then(user =>
			{
				user.orders.push({orderId: savedOrder._id});
				return user.save().then((user,error) =>
				{
					if (error) {return false;}
					else {return user;}
				});
			});

			if(savedOrder !== false && updateUser !== false)
			{
				return {message: `Order successfully placed.`, order: savedOrder};
			}
			else
			{
				return "Something went wrong.";
			}
		}
		else
		{
			return "Admin users are not authorized to check out products.";
		}
	}
	catch(error)
	{
		return error.message;
	}
};

// Controller for adding user review
module.exports.review = async(reqParams, userData) =>
{
	return Order.findById(userData.reqBody.orderId).then(order =>
	{
		if(order.userId === userData.userId)
		{
			// search if reqParams.id is in order.products
			const product = order.products.find(product => product.productId === reqParams.id);
			
			// if it is, then save the user reviews
			if(product)
			{
				return Product.findById(reqParams.id).then(product =>
				{
					// setting review name to be posted as username by default
					let reviewName = userData.username

					// check if the user wants to post review as anon, then repalce the username
					if(userData.reqBody.isAnon)
					{
						reviewName = "Anonymous User";
					}

					// check if the user has already posted a review of this product:
					if(product.userFeedback.length > 0)
					{
						const userIndex = product.userFeedback.findIndex((item) => item.userId === userData.userId);
						// if the user has already posted a review, update it
						if(userIndex >= 0)
						{
							product.userFeedback[userIndex].userRating = userData.reqBody.userRating;
							product.userFeedback[userIndex].userReview = userData.reqBody.userReview;

							// updating the averageRating
							let ratingCount = product.userFeedback.length;
							let sumRatings = product.userFeedback.reduce((total, feedback) => total + feedback.userRating, 0);
							let computedAverageRating = sumRatings/ratingCount;

							product.averageRating = computedAverageRating;
							return product.save().then((product,error) =>
							{
								if(error)
								{
									return error.message;
								}
								else
								{
									return "Review has been updated."
								}
							})
						}
					}

					// if not, then post a new review
					else
					{
						return Product.findById(reqParams.id).then(product =>
						{
							// pushing the rating and review to the feedBack array
							product.userFeedback.push({
							userId: userData.userId,
							reviewName: reviewName,
							userRating: userData.reqBody.userRating,
							userReview: userData.reqBody.userReview
							});
							return product.save().then((product,error) =>
							{

								if(error)
								{
									return "Failed to post review.";
								}
								else
								{
									return Product.findById(reqParams.id).then(product =>
									{
										// updating the averageRating
										let ratingCount = product.userFeedback.length;
										let sumRatings = product.userFeedback.reduce((total, feedback) => total + feedback.userRating, 0);
										let computedAverageRating = sumRatings/ratingCount;

										product.averageRating = computedAverageRating;

										return product.save().then((product,error) =>
										{
											if(error)
											{
												return "Failed to post review.";
											}
											else
											{
												return "Review successfully posted.";
											}
										})
									})
								}
							})
						})
					}
				})
				
			}
			else
			{
				let message = Promise.resolve(`Product with ID ${reqParams.id} not found in this order`);
				return message;
			}	
		}
		else
		{
			let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
			return message;
		}
	})
};

// Controller for search and filter function
/*
NOTE: this search function accepts the following filters:
{
	"keyword": "", - keyword that will be searched in the name and description
	"category": "", - category tag of item to filter
	"onSale": "", - filters only items that are on sale
	"minPrice": 0, - minimum price value
	"maxPrice": 1000000000, - maximum price value
	"sortBy": "" - sets the sort order. accepts "price", "reviews", "buyCount"

	only input the fields you want to apply. i.e. if you don't want to filter items on sale, leave the value the same as above template 
}
*/
module.exports.searchProducts = async (reqBody) =>
{
	const query = {};
	const sort = {};

	// Search by keyword in name and description
	if (reqBody.keyword) 
	{
		query.$or = 
		[
			{ name: { $regex: reqBody.keyword, $options: 'i' } },
			{ description: { $regex: reqBody.keyword, $options: 'i' } },
		];
	}

	// Only shows active items
	query.isActive = true;

	// Filter by category
	if (reqBody.category) 
	{
		query.category = reqBody.category;
	}

	// Filter by on sale
	if (reqBody.onSale) 
	{
		query.salePercent = { $gt: 0 };
	}

	// Filter by min and max price
	if (reqBody.minPrice) 
	{
		query.price = { $gte:reqBody. minPrice };
	}
	if (reqBody.maxPrice) 
	{
		query.price = { ...query.price, $lte: reqBody.maxPrice };
	}

	// Sort by price or averageRating
	if (reqBody.sortBy) 
	{
		if (reqBody.sortBy === 'price') 
		{
			sort.price = 1; // Ascending order
		} 
		else if (reqBody.sortBy === 'reviews') 
		{
			sort.averageRating = -1; // Descending order
		}
		else if (reqBody.sortBy === 'buyCount') 
		{
			sort.buyCount = -1; // Descending order
		}
	}

	// Find products that match the query and sort them
	const products = await Product.find(query).sort(sort);

	return products;
};

// Controller for getting the list of all categories
module.exports.getCategories = () =>
{
	return Product.distinct("category");
}

// Controller for getting all products (Admin only)
module.exports.getAllProducts = () =>
{
	return Product.find().then(result =>{return result;});
}