// Dependencies
const Order = require("../models/Order");


// Controller function for retrieving all orders (Admin Only)
module.exports.getAllOrders = () =>
{
	return Order.find().then(orders => {return orders;});
};

// Controller function for updating order status
/*
Note that the only thing the user can do is to confirm receipt. The rest of the statuses are admin only.
*/
module.exports.updateOrderStatus = (reqParams, reqBody) =>
{
	// Only the user should be able to confirm receipt of the order
	if(reqBody.orderStatus == 5)
	{
		return Promise.resolve(`Only the user is authorized to confirm receipt of order.
		Please see below list of accepted values:
		0 - To Ship
		1 - Packed
		2 - Shipped
		3 - Out for Delivery
		4 - Delivered`);
	}
	else
	{
		let status;
		// saving the status
		if(reqBody.orderStatus == 0)
		{
			status = "To Ship";
		}
		if(reqBody.orderStatus == 1)
		{
			status = "Packed";
		}
		if(reqBody.orderStatus == 2)
		{
			status = "Shipped";
		}
		if(reqBody.orderStatus == 3)
		{
			status = "Out for Delivery";
		}
		if(reqBody.orderStatus == 4)
		{
			status = "Delivered";
		}
		
		return Order.findByIdAndUpdate(reqParams.id,{orderStatus: reqBody.orderStatus, lastUpdatedOn: new Date()}).then(order =>
		{
			console.log (new Date())
			return({message:`The status has been successfully updated to: ${status}`});
		})
	}	
};

// Controller function for user confirming receipt
module.exports.confirmReceipt = (reqParams, userData) =>
{
	

	return Order.findById(reqParams.id).then(order =>
	{
		if(order.orderStatus >= 4)
		{
			if(userData.id === order.userId)
			{
				return Order.findByIdAndUpdate(reqParams.id, {orderStatus : 5, lastUpdatedOn: new Date()}).then(order =>
				{
					return({message:`Thank you for shopping with us! We're glad to hear that you've received your order. We would greatly appreciate it if you could take a moment to rate our products.`});
				});
			}
			else
			{
				let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
				return message;
			}
		}
		else
		{
			return "Error: You cannot confirm receipt of a product that has not been delivered yet."
		}
		
	})
};