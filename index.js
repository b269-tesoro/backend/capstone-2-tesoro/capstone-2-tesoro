// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

// Server
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);



// MongoDB connection
mongoose.connect("mongodb+srv://tesorocelina:admin123@zuitt-bootcamp.ycfxf2m.mongodb.net/capstone-2-tesoro?retryWrites=true&w=majority", {
	// avoids current and future errors while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true,
});

mongoose.connection.once("open", () => console.log("Now connected to cloud database!"));

// Server listening
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));