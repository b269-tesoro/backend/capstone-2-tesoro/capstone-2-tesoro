// Dependencies
const express = require("express");
const router = express.Router();
module.exports = router;

const orderController = require("../controllers/orderController");
const auth = require("../auth");

// Route for retrieving all orders (Admin Only)
router.get("/", auth.verifyAdmin, (req,res) =>
{
	orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
});

// Route for updating order status (Admin Side)
router.put("/:id/status", auth.verifyAdmin, (req,res) =>
{
	orderController.updateOrderStatus(req.params, req.body /*orderStatus*/).then(resultFromController => res.send(resultFromController));
});

// Route for updating order status (Admin Side)
router.put("/:id/confirm", auth.verify, (req,res) =>
{
	const userData = auth.decode(req.headers.authorization);

	orderController.confirmReceipt(req.params, userData).then(resultFromController => res.send(resultFromController));
});