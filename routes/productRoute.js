// Dependencies
const express = require("express");
const router = express.Router();
module.exports = router;

const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for new product creation (admin only)
router.post("/new", auth.verifyAdmin, (req,res) =>
{
	productController.createNewProduct(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/", (req,res) =>
{
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific product
router.get("/product/:productId", (req,res) =>
{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating specific product (Admin only)
router.put("/:productId/update", auth.verifyAdmin, (req,res) =>
{
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for archiving specific product (Admin only)
router.patch("/:productId/archive", auth.verifyAdmin, (req,res) =>
{
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products (Admin only)
router.get("/archives/all", auth.verifyAdmin, (req,res) =>
{
	productController.getArchivedProducts().then(resultFromController => res.send(resultFromController));
});

// Route for restoring specific product (Admin only)
router.patch("/:productId/restore", auth.verifyAdmin, (req,res) =>
{
	productController.restoreProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for buying product immediately from page without adding to cart (Non-Admin)
router.post("/:productId/buynow", auth.verify, (req,res) =>
{
	let data =
	{
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body //quantity
	}
	productController.buyNow(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for product reviews
router.put("/:id/review", auth.verify, (req,res) =>
{
	const userData = 
	{
		userId: auth.decode(req.headers.authorization).id,
		username: auth.decode(req.headers.authorization).username,
		reqBody: req.body //orderId, isAnon, userRating, userReview
	}
	productController.review(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for product search
router.post("/search", (req,res) =>
{
	productController.searchProducts(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for category list
router.get("/categories", (req,res) =>
{
	productController.getCategories().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all products (Admin Only)
router.get("/all", auth.verifyAdmin, (req,res) =>
{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});