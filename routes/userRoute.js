// Dependencies
const express = require("express");
const router = express.Router();
module.exports = router;

const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for new user registration
router.post("/register", (req,res) =>
{
	userController.registerNewUser(req.body /*email, username, password*/).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req,res) =>
{
	userController.userLogin(req.body /*username or email, password*/).then(resultFromController => res.send(resultFromController));
});

// Route for accessing user profile
router.post("/:id/profile", auth.verify, (req,res) =>
{
	const userData = auth.decode(req.headers.authorization);

	userController.getUserProfile(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for accessing all users (admin only)
router.get("/all", auth.verifyAdmin, (req,res) =>
{
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// Route for elevating user to admin status (admin only)
router.post("/:id/elevate", auth.verifyAdmin, (req, res) =>
{
	userController.elevate(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a user's orders
router.post("/:id/orders", auth.verify, (req, res) =>
{
	const userData = auth.decode(req.headers.authorization)
	userController.getUserOrders(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for adding to cart and updating cart(Non-Admin)
router.put("/:id/cart/add", auth.verify, (req,res) =>
{
	let data =
	{
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body //productId and quantity
	}
	userController.addToCart(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for removing item from cart (Non-Admin)
router.put("/:id/cart/remove", auth.verify, (req,res) =>
{
	let data =
	{
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body //productId
	}
	userController.removeFromCart(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for accessing user cart
router.post("/:id/cart", auth.verify, (req,res) =>
{
	const userData = auth.decode(req.headers.authorization);

	userController.getCart(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for checking out all items in user's cart
router.post("/:id/cart/checkout", auth.verify, (req,res) =>
{
	const userData = auth.decode(req.headers.authorization);

	userController.checkOutCart(req.params, userData).then(resultFromController => res.send(resultFromController));
});