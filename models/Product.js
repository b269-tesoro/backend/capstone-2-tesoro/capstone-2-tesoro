const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
{
	name:
	{
		type: String,
		required: [true, "name is required."]
	},
	description:
	{
		type: String,
		required: [true, "description is required."]
	},
	price:
	{
		type: Number,
		required: [true, "base price is required."]
	},
	category:
	{
		type: [String],
		required: [true, "at least one category is required."]
	},
	salePercent:
	{
		type: Number,
		default: 0
	},
	finalPrice:
	{
		// to be computed in controller
		type: Number,
	},
	stock:
	{
		type: Number,
		required: [true, "stock price is required."]
	},
	buyCount:
	{
		type: Number,
		default: 0
	},
	userFeedback:
	[{
		userId:
		{
			type: String
		},
		reviewName:
		{
			type: String
		},
		userRating:
		{
			type: Number,
			required: [true, "rating is required."],
			min: 1,
			max: 5,
			validate:
			{
				validator: Number.isInteger,
			message: `Invalid value for userRating. The accepted values are:
			1 - Poor
			2 - Could Be Better
			3 - Decent
			4 - Good
			5 - Excellent`
			}
		},
		userReview:
		{
			type: String
		},
		postedOn:
		{
			type: Date,
			default: new Date()
		}
	}],
	averageRating:
	{
		type: Number
	},
	isActive:
	{
		type: Boolean,
		default: true
	},
	createdOn:
	{
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Product", productSchema)