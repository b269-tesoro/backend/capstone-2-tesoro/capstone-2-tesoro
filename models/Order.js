const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
{
	userId:
	{
		type: String,
		required: [true, "userId is required."]
	},
	// because the products can change and be updated anytime, the Order list needs to take a snapshot of the information of the product at the time of purchase to ensure accurate records of the transaction. This is why the Order model takes more than just the productId and quantity, unlike the User cart which takes direct reference to the Product model.
	products:
	[{
		productId:
		{
			type: String,
		},
		name:
		{
			type: String,
		},
		description:
		{
			type: String,
		},
		price:
		{
			type: Number,
		},
		salePercent:
		{
			type: Number,
			default: 0
		},
		quantity:
		{
			type: Number,
			default: 1
		},
		finalPrice:
		{
			type: Number,
			
		},
		subTotal:
		{
			type: Number,
		}
	}],
	totalAmount:
	{
		type: Number,
	},
	purchasedOn:
	{
		type : Date,
		default : new Date() 
	},
	orderStatus:
	{
		type: Number,
		default: 0,
		min: 0,
		max: 5,
		validate:
		{
			validator: Number.isInteger,
			message: `Invalid value for oderStatus. The accepted values are:
			0 - To Ship
			1 - Packed
			2 - Shipped
			3 - Out for Delivery
			4 - Delivered
			5 - Confirmed Receipt`
		}
	},
	lastUpdatedOn:
	{
		type: Date
	}
});

module.exports = mongoose.model("Order", orderSchema)