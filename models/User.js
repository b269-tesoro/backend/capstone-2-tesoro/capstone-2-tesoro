const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
{
	email:
	{
		type: String,
		required: [true, "email is required."]
	},
	username:
	{
		type: String,
		required: [true, "username is required."]
	},
	password:
	{
		type: String,
		required: [true, "password is required."]
	},
	cart:
	[{
		productId:
		{
			type: mongoose.Schema.Types.ObjectId,
			// The ref parameter allows the cart to access real time product data instead of copying them one by one from the product. I have decided to use this to make sure that if for example, an item goes on sale or updates name or description, the changes are reflected in the cart. This is more realistic and prevents issues if the user is the type to let items linger in their cart and wait for a sale to check out.
			ref: "Product"		
		},
		quantity:
		{
			type: Number,
			default: 1
		},
		subTotal:
		{
			// computed in the controller
			type: Number
		}
	}],
	orders:
	[{
		orderId:
		{
			type: String,
		}
	}],
	isAdmin:
	{
		type: Boolean,
		default: false
	},
	registeredOn:
	{
		type: Date,
		default: new Date()
	}
});


module.exports = mongoose.model("User", userSchema)